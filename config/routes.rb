Rails.application.routes.draw do
  resources :configuracoes
  resources :fidelidades
  resources :vendas
  resources :compras
  resources :estoques
  resources :promissorias
  resources :fornecedores
  resources :notafornecedores
  resources :promissoria
  resources :nota
  devise_for :usuarios
  resources :movimentos do
    get 'fluxo',            on: :collection
    get 'comprovante',      on: :collection
    get 'newdespesa',       on: :collection
    get 'despesapendente',  on: :collection
    get 'dre',              on: :collection
    get 'toogle_status',    on: :member
  end
  get   'clientes/busca'
  get   'clientes/fidelidade_busca'
  resources :clientes do
    get 'venda',              on: :member
    get 'ultimoagendamento',  on: :collection
    get 'agendamento',        on: :collection
  end
  resources :modopagamentos
  resources :categoriamovimentos
  get 'estatisticas' => 'estatisticas#index'
  root to: 'welcome#index'
  get 'welcome/index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
