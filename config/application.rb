require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Sistemafernanda
  class Application < Rails::Application

    config.app_generators.scaffold_controller :responders_controller

    # config.assets.paths << Rails.root.join('app', 'vendor','assets', 'fonts')

    config.time_zone = 'Brasilia'
    Time::DATE_FORMATS[:default] = "%d/%m/%Y %H:%M"
    Date::DATE_FORMATS[:default] = "%d/%m/%Y"
    config.i18n.default_locale = 'pt-BR'

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded
    #config.generators do |g|
    #        g.stylesheets false
    #        g.javascripts false
    #end
  end
end
