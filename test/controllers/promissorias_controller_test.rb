require 'test_helper'

class PromissoriasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @promissoria = promissorias(:one)
  end

  test "should get index" do
    get promissorias_url
    assert_response :success
  end

  test "should get new" do
    get new_promissoria_url
    assert_response :success
  end

  test "should create promissoria" do
    assert_difference('Promissoria.count') do
      post promissorias_url, params: { promissoria: { data_vencimento: @promissoria.data_vencimento, descricao: @promissoria.descricao, valor_debito: @promissoria.valor_debito } }
    end

    assert_redirected_to promissoria_url(Promissoria.last)
  end

  test "should show promissoria" do
    get promissoria_url(@promissoria)
    assert_response :success
  end

  test "should get edit" do
    get edit_promissoria_url(@promissoria)
    assert_response :success
  end

  test "should update promissoria" do
    patch promissoria_url(@promissoria), params: { promissoria: { data_vencimento: @promissoria.data_vencimento, descricao: @promissoria.descricao, valor_debito: @promissoria.valor_debito } }
    assert_redirected_to promissoria_url(@promissoria)
  end

  test "should destroy promissoria" do
    assert_difference('Promissoria.count', -1) do
      delete promissoria_url(@promissoria)
    end

    assert_redirected_to promissorias_url
  end
end
