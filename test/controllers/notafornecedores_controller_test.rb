require 'test_helper'

class NotafornecedoresControllerTest < ActionDispatch::IntegrationTest
  setup do
    @notafornecedor = notafornecedores(:one)
  end

  test "should get index" do
    get notafornecedores_url
    assert_response :success
  end

  test "should get new" do
    get new_notafornecedor_url
    assert_response :success
  end

  test "should create notafornecedor" do
    assert_difference('Notafornecedor.count') do
      post notafornecedores_url, params: { notafornecedor: { data_compra: @notafornecedor.data_compra, descricao: @notafornecedor.descricao, fornecedor: @notafornecedor.fornecedor, total: @notafornecedor.total, valor_devolvido: @notafornecedor.valor_devolvido, valor_retirado: @notafornecedor.valor_retirado } }
    end

    assert_redirected_to notafornecedor_url(Notafornecedor.last)
  end

  test "should show notafornecedor" do
    get notafornecedor_url(@notafornecedor)
    assert_response :success
  end

  test "should get edit" do
    get edit_notafornecedor_url(@notafornecedor)
    assert_response :success
  end

  test "should update notafornecedor" do
    patch notafornecedor_url(@notafornecedor), params: { notafornecedor: { data_compra: @notafornecedor.data_compra, descricao: @notafornecedor.descricao, fornecedor: @notafornecedor.fornecedor, total: @notafornecedor.total, valor_devolvido: @notafornecedor.valor_devolvido, valor_retirado: @notafornecedor.valor_retirado } }
    assert_redirected_to notafornecedor_url(@notafornecedor)
  end

  test "should destroy notafornecedor" do
    assert_difference('Notafornecedor.count', -1) do
      delete notafornecedor_url(@notafornecedor)
    end

    assert_redirected_to notafornecedores_url
  end
end
