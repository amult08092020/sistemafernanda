require 'test_helper'

class FidelidadesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @fidelidade = fidelidades(:one)
  end

  test "should get index" do
    get fidelidades_url
    assert_response :success
  end

  test "should get new" do
    get new_fidelidade_url
    assert_response :success
  end

  test "should create fidelidade" do
    assert_difference('Fidelidade.count') do
      post fidelidades_url, params: { fidelidade: { cliente_id: @fidelidade.cliente_id, quant_compra: @fidelidade.quant_compra } }
    end

    assert_redirected_to fidelidade_url(Fidelidade.last)
  end

  test "should show fidelidade" do
    get fidelidade_url(@fidelidade)
    assert_response :success
  end

  test "should get edit" do
    get edit_fidelidade_url(@fidelidade)
    assert_response :success
  end

  test "should update fidelidade" do
    patch fidelidade_url(@fidelidade), params: { fidelidade: { cliente_id: @fidelidade.cliente_id, quant_compra: @fidelidade.quant_compra } }
    assert_redirected_to fidelidade_url(@fidelidade)
  end

  test "should destroy fidelidade" do
    assert_difference('Fidelidade.count', -1) do
      delete fidelidade_url(@fidelidade)
    end

    assert_redirected_to fidelidades_url
  end
end
