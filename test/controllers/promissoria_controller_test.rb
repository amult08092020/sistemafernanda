require 'test_helper'

class PromissoriaControllerTest < ActionDispatch::IntegrationTest
  setup do
    @promissorium = promissoria(:one)
  end

  test "should get index" do
    get promissoria_url
    assert_response :success
  end

  test "should get new" do
    get new_promissorium_url
    assert_response :success
  end

  test "should create promissorium" do
    assert_difference('Promissorium.count') do
      post promissoria_url, params: { promissorium: { cliente_id: @promissorium.cliente_id, data_compra: @promissorium.data_compra, data_vencimento: @promissorium.data_vencimento, descricao: @promissorium.descricao, valor_total: @promissorium.valor_total } }
    end

    assert_redirected_to promissorium_url(Promissorium.last)
  end

  test "should show promissorium" do
    get promissorium_url(@promissorium)
    assert_response :success
  end

  test "should get edit" do
    get edit_promissorium_url(@promissorium)
    assert_response :success
  end

  test "should update promissorium" do
    patch promissorium_url(@promissorium), params: { promissorium: { cliente_id: @promissorium.cliente_id, data_compra: @promissorium.data_compra, data_vencimento: @promissorium.data_vencimento, descricao: @promissorium.descricao, valor_total: @promissorium.valor_total } }
    assert_redirected_to promissorium_url(@promissorium)
  end

  test "should destroy promissorium" do
    assert_difference('Promissorium.count', -1) do
      delete promissorium_url(@promissorium)
    end

    assert_redirected_to promissoria_url
  end
end
