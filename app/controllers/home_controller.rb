class HomeController < ApplicationController


  def index
    data = Date.today
    aniv = data.strftime('%d/%m')

    semana = Date.today.beginning_of_week..Date.today.end_of_week
    @aniversariantes = Cliente.where(nascimento: aniv)
    #@movimentos = Movimento.where("valor_realizado IS NULL AND data_prevista = ?", semana)
    @movimentos = Movimento.where(data_prevista: semana, data_realizado: nil)
    @lembretes = Lembrete.where(datadealerta: semana)
    @agendamentoretorno = Agendamento.where(retorno: semana)

  end

end
