class PromissoriaController < ApplicationController
  before_action :set_promissorium, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @promissoria = Promissorium.all
    respond_with(@promissoria)
  end

  def show
    respond_with(@promissorium)
  end

  def new
    @promissorium = Promissorium.new
    respond_with(@promissorium)
  end

  def edit
  end

  def create
    @promissorium = Promissorium.new(promissorium_params)
    @promissorium.save
    respond_with(@promissorium)
  end

  def update
    @promissorium.update(promissorium_params)
    respond_with(@promissorium)
  end

  def destroy
    @promissorium.destroy
    respond_with(@promissorium)
  end

  private
    def set_promissorium
      @promissorium = Promissorium.find(params[:id])
    end

    def promissorium_params
      params.require(:promissorium).permit(:descricao, :valor_total, :data_compra, :data_vencimento, :cliente_id)
    end
end
