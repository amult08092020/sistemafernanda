class NotaController < ApplicationController
  before_action :set_notum, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @nota = Notum.all
    respond_with(@nota)
  end

  def show
    respond_with(@notum)
  end

  def new
    @notum = Notum.new
    respond_with(@notum)
  end

  def edit
  end

  def create
    @notum = Notum.new(notum_params)
    @notum.save
    respond_with(@notum)
  end

  def update
    @notum.update(notum_params)
    respond_with(@notum)
  end

  def destroy
    @notum.destroy
    respond_with(@notum)
  end

  private
    def set_notum
      @notum = Notum.find(params[:id])
    end

    def notum_params
      params.require(:notum).permit(:fornecedor, :data_compra, :valor_devolvido, :valor_retirado, :total, :descricao)
    end
end
