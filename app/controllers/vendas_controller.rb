class VendasController < ApplicationController
  before_action :set_venda, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @vendas = Venda.all
    respond_with(@vendas)
  end

  def show
    respond_with(@venda)
  end

  def new
    @venda = Venda.new
    respond_with(@venda)
  end

  def edit
    if params[:cliente_id].present?
      @venda.cliente_id = params[:cliente_id]
    end
  end

  def create
    @venda = Venda.new(venda_params)
    @venda.save
    respond_with(@venda)
  end

  def update
    @venda.update(venda_params)
    respond_with(@venda)
  end

  def destroy
    @venda.destroy
    respond_with(@venda)
  end

  private
  def set_venda
    @venda = Venda.find(params[:id])
  end

  def venda_params
    params.require(:venda).permit(:data_venda, :valor_total, :cliente_id, :descricao,
    compras_attributes: [:id, :venda_id, :cliente_id, :quantidade, :estoque_id, :_destroy])
  end
end
