class CategoriamovimentosController < ApplicationController
  before_action :set_categoriamovimento, only: [:show, :edit, :update, :destroy]

  # GET /categoriamovimentos
  # GET /categoriamovimentos.json
  def index
    @categoriamovimentos = Categoriamovimento.all
  end

  # GET /categoriamovimentos/1
  # GET /categoriamovimentos/1.json
  def show
  end

  # GET /categoriamovimentos/new
  def new
    @categoriamovimento = Categoriamovimento.new
  end

  # GET /categoriamovimentos/1/edit
  def edit
  end

  # POST /categoriamovimentos
  # POST /categoriamovimentos.json
  def create
    @categoriamovimento = Categoriamovimento.new(categoriamovimento_params)

    respond_to do |format|
      if @categoriamovimento.save
        format.html { redirect_to @categoriamovimento, notice: 'Categoriamovimento was successfully created.' }
        format.json { render :show, status: :created, location: @categoriamovimento }
      else
        format.html { render :new }
        format.json { render json: @categoriamovimento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categoriamovimentos/1
  # PATCH/PUT /categoriamovimentos/1.json
  def update
    respond_to do |format|
      if @categoriamovimento.update(categoriamovimento_params)
        format.html { redirect_to @categoriamovimento, notice: 'Categoriamovimento was successfully updated.' }
        format.json { render :show, status: :ok, location: @categoriamovimento }
      else
        format.html { render :edit }
        format.json { render json: @categoriamovimento.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categoriamovimentos/1
  # DELETE /categoriamovimentos/1.json
  def destroy
    @categoriamovimento.destroy
    respond_to do |format|
      format.html { redirect_to categoriamovimentos_url, notice: 'Categoriamovimento was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_categoriamovimento
      @categoriamovimento = Categoriamovimento.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def categoriamovimento_params
      params.require(:categoriamovimento).permit(:nome, :descricao, :movimento_id)
    end
end
