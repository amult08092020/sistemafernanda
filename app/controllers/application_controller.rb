class ApplicationController < ActionController::Base
  # include Pundit

  protect_from_forgery with: :exception
  # protect_from_forgery with: :null_session
  before_action :authenticate_usuario!

  # rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  #
  # private
  #
  # def user_not_authorized
  #   flash[:warning] = "Acesso não autorizado."
  #   redirect_to(request.referrer || root_path)
  # end
  #protect_from_forgery prepend: true
  #
  #	before_action :configure_permitted_parameters, if: :devise_controller?

end
