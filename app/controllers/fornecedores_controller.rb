class FornecedoresController < ApplicationController
  before_action :set_fornecedor, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @fornecedores = Fornecedor.all
    respond_with(@fornecedores)
  end

  def show
    respond_with(@fornecedor)
  end

  def new
    @fornecedor = Fornecedor.new
    respond_with(@fornecedor)
  end

  def edit
  end

  def create
    @fornecedor = Fornecedor.new(fornecedor_params)
    @fornecedor.save
    respond_with(@fornecedor)
  end

  def update
    @fornecedor.update(fornecedor_params)
    respond_with(@fornecedor)
  end

  def destroy
    @fornecedor.destroy
    respond_with(@fornecedor)
  end

  private
    def set_fornecedor
      @fornecedor = Fornecedor.find(params[:id])
    end

    def fornecedor_params
      params.require(:fornecedor).permit(:nome, :cnpj, :telefone, :endereco)
    end
end
