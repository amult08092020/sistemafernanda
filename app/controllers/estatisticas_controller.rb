class EstatisticasController < ApplicationController

  has_scope :contavendas
  def index
    if params[:periodo]
      @ano_referencia = params[:periodo].to_date
    else
      @ano_referencia = Time.now
    end
    # FIXME Acrescentado para evitar bugs nos dias > 28
    #       Deve ser corrigido pois pode afetar o fluxo de caixa do mês
    @ano_referencia = @ano_referencia.change(day: 1)
    @vendas = apply_scopes(Venda).all
  end

end
