class WelcomeController < ApplicationController
  def index

    data = Date.today
    aniv = data
    data_inicio = data.beginning_of_week
    data_fim = data.end_of_week

    semana = data_inicio.cweek

    semana = Date.today.beginning_of_week..Date.today.end_of_week
    @aniversariantes = Cliente.aniversariantes_do_dia
    @aniversariantes_semana = Cliente.aniversariantes_da_semana

    #@movimentos = Movimento.where("valor_realizado IS NULL AND data_prevista = ?", semana)
    # @movimentos = Movimento.where(data_prevista: semana, data_realizado: nil)
    @fidelidade = Movimento.fidelidade_contemplar
    # @lembretes = Lembrete.where(datadealerta: semana
    @promissoria_a_vencer = Promissoria.promissoria_vencer(Date.today)
    @promissoria_vencida = Promissoria.promissoria_vencida(Date.today)

  end
end
