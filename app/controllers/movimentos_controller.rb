class MovimentosController < ApplicationController
  before_action :set_movimento, only: [:show, :edit, :comprovante, :update, :destroy]
  # after_filter { flash.discard if request.xhr? }, only: :toogle_status

  respond_to :html

  has_scope :do_dia, :type => :boolean
  has_scope :data_inicio
  has_scope :data_fim
  has_scope :do_mes, :type => :boolean
  has_scope :da_semana, :type => :boolean
  has_scope :do_tipo, :type => :array
  has_scope :procedimento
  has_scope :despesapendente
  has_scope :movimento_categoria, :type => :boolean
  # GET /movimentos
  # GET /movimentos.json
  def index
    # authorize Movimento, :index?
    params[:data_inicio] = l(Date.today) unless params[:data_inicio]
    params[:data_fim] = l(Date.today) unless params[:data_fim]
    @movimentos = apply_scopes(Movimento).order('id ASC').all
    @saldo = 0
    @totalreceita = @movimentos.receita.collect(&:valor_realizado).sum
    @totaldespesa = @movimentos.despesa.collect(&:valor_realizado).sum
    @contmovimentos = @movimentos.count
    @contreceita = @movimentos.receita.count
    @contdespesa = @movimentos.despesa.count
    @saldo = @totalreceita - @totaldespesa
    @modopagamento = Modopagamento.all
  end

  def fluxo

    if params[:periodo]
      @ano_referencia = params[:periodo].to_date
    else
      @ano_referencia = Date.today
    end
    # FIXME Acrescentado para evitar bugs nos dias > 28
    #       Deve ser corrigido pois pode afetar o fluxo de caixa do mês
    @ano_referencia = @ano_referencia.change(day: 1)
  end

  def dre

    if params[:periodo]
      @ano_referencia = params[:periodo].to_date
    else
      @ano_referencia = Date.today
    end
    # FIXME Acrescentado para evitar bugs nos dias > 28
    #       Deve ser corrigido pois pode afetar o fluxo de caixa do mês
    @ano_referencia = @ano_referencia.change(day: 1)
  end

  # GET /movimentos/1
  # GET /movimentos/1.json
  def show
  end

  def comprovante

  end

  def despesapendente
    mes = Date.today.beginning_of_month .. Date.today.end_of_month
    @movimentos = Movimento.where(data_realizado: nil, data_prevista: mes)
    @totaldespesa = @movimentos.despesa.collect(&:valor_previsto).sum
  end

  def gastoscategorias
    @movimentos = apply_scopes(Movimento).order('id ASC, descricao ASC')
    @categoriamovimento = Categoriamovimento.all
  end

  # GET /movimentos/new
  def new
    @movimento = Movimento.new
    @modopagamento = Modopagamento.all
    if params[:cliente_id].present?
      @movimento.cliente_id = params[:cliente_id]
    end
    if params[:venda_id].present?
      @movimento.venda_id = params[:venda_id]
      @venda = Venda.find(params[:venda_id])
    end
    if params[:promissoria_id].present?
      @movimento.promissoria_ids = params[:promissoria_id]
      @promissoria = Promissoria.find(params[:promissoria_id])
    end
  end

  def newdespesa
    @movimento = Movimento.new
    @categoria = Categoriamovimento.all
  end

  # GET /movimentos/1/edit
  def edit
    @modopagamento = Modopagamento.all
    @categoria = Categoriamovimento.all
    if params[:data_inicio].present?
      @datainicio = params[:data_inicio]
      @datafim = params[:data_fim]
    end
  end

  def editdespesa
    @categoria = Categoriamovimento.all
    if params[:data_inicio].present?
      datainicio = params[:data_inicio]
      datafim = params[:data_fim]
    end
  end

  # POST /movimentos
  # POST /movimentos.json
  def create
    @movimento = Movimento.new(movimento_params)
    @movimento.save
    respond_with(@movimento)
  end

  # PATCH/PUT /movimentos/1
  # PATCH/PUT /movimentos/1.json
  def update
    @movimento.update(movimento_params)
    if @datainicio.present?
      respond_with(@movimento, location: movimentos_path(data_inicio: @datainicio, data_fim: @datafim))
    else
      respond_with(@movimento, location: movimento_path)
    end
  end

  def toogle_status
    @movimento = Movimento.find(params[:id])
    @movimento.update_columns(fidelidade: !@movimento.fidelidade)
    # render(nothing: true)
    respond_to do |format|
      format.js { flash.now[:notice] = "Fidelidade incluida no cliente" }
    end
  end

  # DELETE /movimentos/1
  # DELETE /movimentos/1.json
  def destroy
    @movimento.destroy
    respond_to do |format|
      format.html { redirect_to movimentos_url, notice: 'Movimento excluido com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_movimento
    @movimento = Movimento.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def movimento_params
    params.require(:movimento).permit(:valor_previsto, :data_prevista, :valor_realizado, :data_realizado, :descricao, :tipo, :desconto, :cliente_id, :modopagamento_id, :categoriamovimento_id, :agendamento_id, :pontualidade, :valor_dinheiro, :valor_cartao, :venda_id, :promissoria_ids)
  end
end
