class FidelidadesController < ApplicationController
  before_action :set_fidelidade, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @fidelidades = Fidelidade.all
    respond_with(@fidelidades)
  end

  def show
    respond_with(@fidelidade)
  end

  def new
    @fidelidade = Fidelidade.new
    @fidelidade.cliente_id = params[:cliente_id]
    respond_with(@fidelidade)
  end

  def edit
  end

  def create
    @fidelidade = Fidelidade.new(fidelidade_params)
    @fidelidade.save
    respond_with(@fidelidade, location: cliente_path(@fidelidade.cliente_id))
  end

  def update
    @fidelidade.update(fidelidade_params)
    respond_with(@fidelidade)
  end

  def destroy
    @fidelidade.destroy
    respond_with(@fidelidade)
  end

  private
  def set_fidelidade
    @fidelidade = Fidelidade.find(params[:id])
  end

  def fidelidade_params
    params.require(:fidelidade).permit(:quant_compra, :cliente_id)
  end
end
