// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
// turbolinks
// require admin/01jquery.min.js
//= require admin/02bootstrap.min.js
//= require admin/03jquery.mCustomScrollbar.concat.min.js
//= require admin/jquery.mCustomScrollbar.concat.min.js
//= require admin/04waves.min.js
//= require admin/jquery-maskmoney.js
//= require admin/05bootstrap-growl.min.js
//= require admin/06functions.js
//= require admin/07actions.js
//= require admin/08demo.js
//= require moment
//= require fullcalendar
//= require fullcalendar/lang/pt
//= require admin/09input-mask.min.js
//= require admin/bootstrap-toggle.min.js
//= require admin/10bootstrap-datetimepicker.min.js
//= require admin/11bootstrap-select.js
//= require admin/12chosen.jquery.min.js
//= require agendamentos.js

//= require chartkick
//= require jquery_nested_form
