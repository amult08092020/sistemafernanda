class Venda < ApplicationRecord
  belongs_to :cliente, optional: true

  belongs_to :cliente
  has_many :compras
  accepts_nested_attributes_for :compras, :allow_destroy => true

  usar_como_dinheiro :valor_total

  has_many :movimento

  scope :contavendas, -> (periodo) { where(data_venda: periodo) }
  scope :contavendas_movimento, -> (periodo) { joins(:movimento).where(data_venda: periodo) }

  # after_save :soma_total_venda
  # def soma_total_venda
  #   valor_total_compra = Compra.find().valor_total
  #   self.valor_total = valor_total_compra
  #   puts "Valor total da venda #{self.valor_total}"
  # end

end
