class Compra < ApplicationRecord

  belongs_to :cliente, optional: true
  belongs_to :venda, optional: true

  belongs_to :estoque, optional: true

  scope :busca_compra_venda, -> (id) { where(venda_id: id) }

  after_save :soma_total_venda
  def soma_total_venda
    total_venda = Compra.busca_compra_venda(self.venda_id).collect(&:valor_total).sum
    venda = Venda.find(self.venda_id)
    venda.update :valor_total => total_venda
  end

  before_save :soma_total
  def soma_total
    valor_unico = Estoque.find(self.estoque_id).valordevenda
    self.valor_total = valor_unico * self.quantidade
  end

  after_create :retirar_estoque
  def retirar_estoque
    estoque = Estoque.find(self.estoque_id)
    @quantidade_restante = estoque.quantidade - self.quantidade
    estoque.update :quantidade => @quantidade_restante
  end

  before_update :atualiza_retirada_estoque
  def atualiza_retirada_estoque
    estoque = Estoque.find(self.estoque_id)
    venda = Compra.find(self.id)
    @quantidade_estoque = estoque.quantidade
    if venda.quantidade > self.quantidade
      sobra = venda.quantidade - self.quantidade
      @quantidade_restante = @quantidade_estoque + sobra
      estoque.update :quantidade => @quantidade_restante
    elsif venda.quantidade < self.quantidade
      sobra = self.quantidade - venda.quantidade
      @quantidade_restante = @quantidade_estoque - sobra
      estoque.update :quantidade => @quantidade_restante
    end
  end

end
