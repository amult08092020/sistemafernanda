class Fidelidade < ApplicationRecord

  belongs_to :cliente

  scope :contafidelidade, -> (id) { where(cliente_id: id) }

end
