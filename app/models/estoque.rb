class Estoque < ApplicationRecord

  validates :nome_produto, :quantidade, :valor,  presence: true
  # usar_como_dinheiro :valor
  # usar_como_dinheiro :totalproduto

  has_many :compra

  belongs_to :notafornecedor, optional: true

  scope :estoque_atual, -> { where("quantidade > 0") }

  before_save :atualiza_valor_venda
  def atualiza_valor_venda
    self.lucro = self.valor * (self.porcentagemlucro.to_f / 100)
    self.valordevenda = self.lucro + self.valor
    self.totalproduto = self.quantidade * self.valor
  end

end
