class Movimento < ApplicationRecord

  extend Enumerize

  validates :valor_previsto, :data_prevista, :tipo, presence: true

  usar_como_dinheiro :valor_previsto
  usar_como_dinheiro :valor_realizado
  usar_como_dinheiro :desconto
  usar_como_dinheiro :valor_cartao
  usar_como_dinheiro :valor_dinheiro

  has_many :pagamento
  has_many :promissorias, through: :pagamento
  belongs_to :cliente
  belongs_to :venda, optional: true
  belongs_to :categoriamovimento, optional: true
  belongs_to :agendamento, optional: true
  belongs_to :modopagamento, optional: true

  scope :conta_fidelidade, -> (id) { joins(:cliente).where(fidelidade: true, cliente_id: id) }

  before_save :altera_valor
  def altera_valor
    if self.desconto.present?
      desconto_passado = self.desconto
      self.valor_realizado = self.valor_previsto - desconto_passado
    end
    if self.valor_dinheiro.present? && self.valor_dinheiro > 0
      self.valor_cartao = self.valor_realizado - self.valor_dinheiro
      desconto_cartao = self.valor_cartao * (self.modopagamento.porcentagem.to_f / 100)
      valor_apos = self.valor_cartao.to_f - desconto_cartao
      self.valor_cartao = valor_apos
      self.valor_realizado = self.valor_cartao + self.valor_dinheiro
    elsif self.modopagamento_id.present?
      desconto_cartao =  self.valor_realizado * (self.modopagamento.porcentagem.to_f / 100)
      valor_apos = self.valor_realizado.to_f - desconto_cartao
      self.valor_realizado = valor_apos
    end
  end

  after_save :seta_valor_devedor
  def seta_valor_devedor
    if self.promissoria_ids.present?
      self.promissoria_ids.each do |promi|
        @promissoria = Promissoria.find(promi)
        restante = @promissoria.valor_restante - self.valor_realizado
        @promissoria.update :valor_restante => restante
      end
    end
  end

  # after_save :seta_movimentoid
  # def seta_movimentoid
  #   if params[:promissoria_id].present?
  #     @agendamento = Promissoria.find(self.promissoria_id)
  #     @agendamento.update movimento_id: self.id
  #   end
  # end

  scope :ultimo_agendamento, -> (cliente) { where(cliente_id: cliente) }

  scope :do_tipo, -> (tipo_arr) {
    if tipo_arr.include?("receita") && tipo_arr.include?("despesa")
    elsif tipo_arr.include?("receita")
      receita
    elsif tipo_arr.include?("despesa")
      despesa
    end
  }

  scope :cartaovalor, -> (cartao) { where(categoriamovimento_id: cartao) }
  scope :movimento_categoria, -> (hoje = Date.today) { where(data_realizado: hoje.beginning_of_month .. hoje.end_of_month, tipo: :despesa ) }
  scope :do_periodo, -> (inicio,fim) { where(data_realizado: inicio..fim) }
  scope :do_dia, -> (dia = Date.today) { do_periodo(dia,dia) }
  scope :data_inicio, -> (inicio) { where("data_realizado >= ?", inicio.to_date) }
  scope :data_fim, -> (fim) { where("data_realizado <= ?", fim.to_date) }
  scope :da_semana, -> (dia) { do_periodo(dia.beginning_of_week,dia_end_of_week) }
  scope :do_mes, -> (diames = Date.today) { do_periodo(diames.beginning_of_month, diames.end_of_month) }
  scope :aberto,     Proc.new { where('data_realizado is null') }
  scope :fidelidade_contemplar, -> { find_by_sql("SELECT nome,COUNT(nome) as total FROM movimentos
                                                  INNER JOIN clientes ON movimentos.cliente_id = clientes.id
                                                  WHERE fidelidade=true GROUP BY nome
                                                  ") }

  scope :pontual, -> (id) { where(cliente_id: id, pontualidade: "pontual") }
  scope :atrasada, -> (id) { where(cliente_id: id, pontualidade: "atrasou") }
  scope :pontualidadegrafico, -> { where(pontualidade: ["atrasou", "pontual"]) }

  scope :procedimento, -> { where(agendamento_id: params[:agendamento_id]) }

  scope :receita, -> { where(tipo: :receita) }
  scope :despesa, -> { where(tipo: :despesa) }

  def self.saldo_inicial(periodo, tipo_relatorio = "")
    ultimo_dia = periodo - 1.day

    receitas_realizadas = Movimento.receita.where('data_realizado <= ?', ultimo_dia).sum(:valor_realizado)
    despesas_realizadas = Movimento.despesa.where('data_realizado <= ?', ultimo_dia).sum(:valor_realizado)
    saldo_realizado     = receitas_realizadas - despesas_realizadas

    hoje = Date.today
    if ultimo_dia < hoje
      saldo_realizado
    else
      receitas_futuras = Movimento.aberto.receita.where('data_prevista between ? and ?', hoje, ultimo_dia).sum(:valor_realizado)
      despesas_futuras = Movimento.aberto.despesa.where('data_prevista between ? and ?', hoje, ultimo_dia).sum(:valor_realizado)
      saldo_futuro     = receitas_futuras - despesas_futuras

      saldo_realizado + saldo_futuro
    end
  end

  def self.total_movimentos(efeito, periodo, tipo_relatorio="DRE", tipo=nil, modopagamento=nil)
    competencia_hoje    = Date.today.strftime '%Y%m'
    competencia_periodo = periodo.strftime '%Y%m'

    if tipo_relatorio == "DRE"
      movimentos_no_periodo = Movimento.where('data_realizado = ?', periodo)
    elsif  tipo_relatorio == "FLUXOCAIXA"
      movimentos_no_periodo = Movimento.where('data_realizado between ? and ? ', periodo.beginning_of_month, periodo.end_of_month)
    end

    if efeito != 'todos'
      movimentos_no_periodo = movimentos_no_periodo.where('tipo = ?', efeito)
    end

    if tipo
      movimentos_no_periodo = movimentos_no_periodo.where('categoriamovimento_id = ?', tipo.to_s)
    end

    if modopagamento
      movimentos_no_periodo = movimentos_no_periodo.where('modopagamento_id = ?', modopagamento)
    end

    if competencia_periodo < competencia_hoje
      movimentos_no_periodo.sum(:valor_realizado)
    else
      valor_previsto  = movimentos_no_periodo.where('data_realizado is null and data_realizado > ?', Date.yesterday).sum(:valor_previsto)
      valor_realizado = movimentos_no_periodo.where('data_realizado is not null').sum(:valor_realizado)
      valor_previsto + valor_realizado
    end
  end

  def self.total_receitas(periodo, tipo_relatorio, tipo=nil, procedimento=nil)
    total_movimentos(:receita, periodo, tipo_relatorio, tipo, procedimento)
  end

  def self.total_despesas(periodo, tipo_relatorio, tipo=nil)
    total_movimentos(:despesa, periodo, tipo_relatorio, tipo)
  end

  def self.saldo_final(periodo, tipo_relatorio)
    total_receitas(periodo, tipo_relatorio) - total_despesas(periodo, tipo_relatorio)
  end

  def self.saldo_total(periodo, tipo_relatorio)
    saldo_inicial(periodo, tipo_relatorio) + saldo_final(periodo, tipo_relatorio)
  end

  def self.ver_pontuacao(cliente)
    @fidelidade = Configuracao.first
    @pontos_fidelidade = Movimento.conta_fidelidade(cliente).count
    @total = @pontos_fidelidade - cliente.fidelidade.count * 5
  end
  def self.ver_pontos(valor,cliente)
    @total = valor - Cliente.busca_cliente_fidelidade(cliente).count * 5
  end

end
