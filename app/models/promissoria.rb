class Promissoria < ApplicationRecord

  validates :descricao, :data_vencimento, :valor_debito, presence: true

  # usar_como_dinheiro :valor_debito

  belongs_to :cliente
  # belongs_to :movimento, optional: true
  has_many :pagamento
  has_many :movimentos, through: :pagamento

  scope :promissoria_vencer, -> (data) { where(data_vencimento: data.beginning_of_week .. data.end_of_week) }
  scope :promissoria_vencida, -> (data) { where("data_vencimento <? AND valor_restante >?", data, 0) }

  self.per_page = 30
end
