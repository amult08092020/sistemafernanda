class Fornecedor < ApplicationRecord

  validates :nome,  presence: true

  has_many :notafornecedor,  dependent: :destroy

end
