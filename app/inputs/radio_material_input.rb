class RadioMaterialInput < SimpleForm::Inputs::CollectionInput
  def input(wrapper_options = nil)
    label_method, value_method = detect_collection_methods
    template.content_tag(:div, class: '') do
      template.concat( @builder.collection_radio_buttons(attribute_name, collection, value_method, label_method, {item_wrapper_tag: :label, item_wrapper_class: "radio radio-inline m-r-20"}, {}) do |b|
   b.radio_button + template.content_tag(:i, nil, class: 'input-helper').html_safe + b.text 
end)
    end
  end
end


