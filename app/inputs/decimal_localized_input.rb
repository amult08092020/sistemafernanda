class DecimalLocalizedInput < SimpleForm::Inputs::Base
  enable :placeholder

  def input(wrapper_options = nil)
    #merged_input_options = merge_wrapper_options(input_html_options, wrapper_options)
    #@builder.text_field(attribute_name, merged_input_options)
    @builder.text_field(attribute_name, input_html_options)
  end

  def input_html_options
    input_html_options = super

    number_options = I18n.t(:'number.format')
    separator = number_options[:separator]
    delimiter = number_options[:delimiter]
    precision = number_options[:precision]
    opts = { :separator => separator, :delimiter => delimiter, :precision => precision }

    input_html_options[:value] = ActionController::Base.helpers.number_with_precision(attribute_value, opts) if attribute_value
    input_html_options
  end

  protected

  def attribute_value
    @value ||= object.send(attribute_name)
  end
end
