class CheckMaterialInput < SimpleForm::Inputs::CollectionInput
  def input(wrapper_options = nil)
    tag_name = "#{@builder.object_name}[#{attribute_name}]"

    template.content_tag(:div, class: '') do
      template.hidden_field_tag(tag_name, false) + 
      template.check_box_tag(tag_name, options['value'] || 1, @builder.object.send("#{attribute_name}"), options) + 
      template.content_tag(:i, nil, class: 'input-helper').html_safe
    end
  end

end


