module MovimentosHelper

  def class_valor(valor)
    estilo = if valor == 0
               'zero'
             elsif valor > 0
               'positivo'
             else
               'negativo'
             end
    "class='#{estilo}'".html_safe
  end

  def class_valor_confirmado(movimento)
    if movimento.confirmado?
      movimento.is_a?(Despesa) ? "text-danger" : "text-success"
    end
  end

  def class_valor_total
    if @saldo <= 0
      "bgm-red"
    elsif @saldo > 1
      "bgm-green"
    end
  end
end
