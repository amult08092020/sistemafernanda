json.extract! estoque, :id, :nome_produto, :descricao, :valor, :quantidade, :created_at, :updated_at
json.url estoque_url(estoque, format: :json)