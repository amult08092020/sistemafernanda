json.array!(@modopagamentos) do |modopagamento|
  json.extract! modopagamento, :id, :tipo, :porcentagem
  json.url modopagamento_url(modopagamento, format: :json)
end
