json.extract! notafornecedor, :id, :fornecedor, :data_compra, :valor_devolvido, :valor_retirado, :total, :descricao, :created_at, :updated_at
json.url notafornecedor_url(notafornecedor, format: :json)