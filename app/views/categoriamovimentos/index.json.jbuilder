json.array!(@categoriamovimentos) do |categoriamovimento|
  json.extract! categoriamovimento, :id, :nome, :descricao, :movimento_id
  json.url categoriamovimento_url(categoriamovimento, format: :json)
end
