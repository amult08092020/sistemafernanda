json.extract! compra, :id, :valor_total, :status, :venda_id, :cliente_id, :created_at, :updated_at
json.url compra_url(compra, format: :json)