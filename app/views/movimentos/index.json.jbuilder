json.array!(@movimentos) do |movimento|
  json.extract! movimento, :id, :valor_previsto, :data_prevista, :valor_realizado, :data_realizado, :descricao, :tipo, :cliente_id
  json.url movimento_url(movimento, format: :json)
end
