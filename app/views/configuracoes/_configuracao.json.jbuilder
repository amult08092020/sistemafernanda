json.extract! configuracao, :id, :fidelidade_valor, :created_at, :updated_at
json.url configuracao_url(configuracao, format: :json)