json.extract! promissorium, :id, :descricao, :valor_total, :data_compra, :data_vencimento, :cliente_id, :created_at, :updated_at
json.url promissorium_url(promissorium, format: :json)