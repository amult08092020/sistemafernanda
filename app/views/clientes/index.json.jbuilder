json.array!(@clientes) do |cliente|
  json.extract! cliente, :id, :nome, :email, :telefone, :cpf, :observacao
  json.url cliente_url(cliente, format: :json)
end
