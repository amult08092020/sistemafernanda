json.extract! notum, :id, :fornecedor, :data_compra, :valor_devolvido, :valor_retirado, :total, :descricao, :created_at, :updated_at
json.url notum_url(notum, format: :json)