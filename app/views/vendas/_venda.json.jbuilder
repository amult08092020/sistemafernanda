json.extract! venda, :id, :data_venda, :valor_total, :created_at, :updated_at
json.url venda_url(venda, format: :json)