json.extract! fornecedor, :id, :nome, :cnpj, :telefone, :endereco, :created_at, :updated_at
json.url fornecedor_url(fornecedor, format: :json)