class AddColumnClienteIdToPromissorias < ActiveRecord::Migration[5.0]
  def change
    add_column :promissorias, :cliente_id, :integer
  end
end
