class AddColumnDescontoToMovimentos < ActiveRecord::Migration[5.0]
  def change
    add_column :movimentos, :desconto, :decimal
  end
end
