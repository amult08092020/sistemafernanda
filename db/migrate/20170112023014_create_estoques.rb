class CreateEstoques < ActiveRecord::Migration[5.0]
  def change
    create_table :estoques do |t|
      t.string :nome_produto
      t.text :descricao
      t.decimal :valor
      t.integer :quantidade

      t.timestamps
    end
  end
end
