class CreatePromissoria < ActiveRecord::Migration[5.0]
  def change
    create_table :promissoria do |t|
      t.text :descricao
      t.decimal :valor_total
      t.date :data_compra
      t.date :data_vencimento
      t.integer :cliente_id

      t.timestamps
    end
  end
end
