class CreatePagamentos < ActiveRecord::Migration[5.0]
  def change
    create_table :pagamentos do |t|
      t.integer :movimento_id
      t.integer :promissoria_id
      t.decimal :valor_devedor

      t.timestamps
    end
  end
end
