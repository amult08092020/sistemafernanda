class AddColumnVendaIdToMovimentos < ActiveRecord::Migration[5.0]
  def change
    add_column :movimentos, :venda_id, :integer
  end
end
