class CreateModopagamentos < ActiveRecord::Migration[5.0]
  def change
    create_table :modopagamentos do |t|
      t.string :tipo
      t.decimal :porcentagem

      t.timestamps
    end
  end
end
