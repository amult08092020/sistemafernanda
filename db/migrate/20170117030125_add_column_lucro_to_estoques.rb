class AddColumnLucroToEstoques < ActiveRecord::Migration[5.0]
  def change
    add_column :estoques, :lucro, :numeric
  end
end
