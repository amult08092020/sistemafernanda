class AddColumnDescricaoToVendas < ActiveRecord::Migration[5.0]
  def change
    add_column :vendas, :descricao, :text
  end
end
