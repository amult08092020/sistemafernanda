class CreateLembretes < ActiveRecord::Migration[5.0]
  def change
    create_table :lembretes do |t|
      t.text :texto
      t.date :datadealerta

      t.timestamps
    end
  end
end
