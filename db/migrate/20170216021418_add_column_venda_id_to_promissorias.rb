class AddColumnVendaIdToPromissorias < ActiveRecord::Migration[5.0]
  def change
    add_column :promissorias, :venda_id, :integer
  end
end
