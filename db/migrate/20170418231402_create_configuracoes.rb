class CreateConfiguracoes < ActiveRecord::Migration[5.0]
  def change
    create_table :configuracoes do |t|
      t.integer :fidelidade_valor

      t.timestamps
    end
  end
end
