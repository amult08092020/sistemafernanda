class AddColumnClienteIdToVendas < ActiveRecord::Migration[5.0]
  def change
    add_column :vendas, :cliente_id, :integer
  end
end
