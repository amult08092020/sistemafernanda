class AddColumnModopagamentoIdToMovimentos < ActiveRecord::Migration[5.0]
  def change
    add_column :movimentos, :modopagamento_id, :integer
  end
end
