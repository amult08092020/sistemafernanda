class CreateFidelidades < ActiveRecord::Migration[5.0]
  def change
    create_table :fidelidades do |t|
      t.integer :quant_compra
      t.integer :cliente_id

      t.timestamps
    end
  end
end
