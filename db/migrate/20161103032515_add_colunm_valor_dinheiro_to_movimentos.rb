class AddColunmValorDinheiroToMovimentos < ActiveRecord::Migration[5.0]
  def change
    add_column :movimentos, :valor_dinheiro, :numeric
    add_column :movimentos, :valor_cartao, :numeric
  end
end
