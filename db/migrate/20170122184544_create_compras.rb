class CreateCompras < ActiveRecord::Migration[5.0]
  def change
    create_table :compras do |t|
      t.numeric :valor_total
      t.string :status
      t.integer :venda_id
      t.integer :cliente_id

      t.timestamps
    end
  end
end
