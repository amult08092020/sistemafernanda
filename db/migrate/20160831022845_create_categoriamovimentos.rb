class CreateCategoriamovimentos < ActiveRecord::Migration[5.0]
  def change
    create_table :categoriamovimentos do |t|
      t.string :nome
      t.text :descricao
      t.integer :movimento_id

      t.timestamps
    end
  end
end
