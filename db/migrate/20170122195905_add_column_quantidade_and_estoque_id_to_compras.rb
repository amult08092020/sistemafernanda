class AddColumnQuantidadeAndEstoqueIdToCompras < ActiveRecord::Migration[5.0]
  def change
    add_column :compras, :quantidade, :integer
    add_column :compras, :estoque_id, :integer
  end
end
