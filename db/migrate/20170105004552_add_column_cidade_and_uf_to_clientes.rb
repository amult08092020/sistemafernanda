class AddColumnCidadeAndUfToClientes < ActiveRecord::Migration[5.0]
  def change
    add_column :clientes, :cidade, :string
    add_column :clientes, :uf, :string
  end
end
